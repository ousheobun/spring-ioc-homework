package cn.oushaobin.spring.base;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringBeanFactory {
	
	private static BeanFactory beanFactory ;
	
	static{
		beanFactory = new ClassPathXmlApplicationContext("config/beans.xml");
	}
	
	public static Object getBean(String beanId){
		return beanFactory.getBean(beanId);
	}
	
	public static Object getBena(String beanId, Object... params){
		return beanFactory.getBean(beanId, params);
	}
	

}
