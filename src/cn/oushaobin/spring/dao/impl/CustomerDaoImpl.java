package cn.oushaobin.spring.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import cn.oushaobin.spring.dao.CustomerDao;
import cn.oushaobin.spring.entity.CustomerEntity;

@Repository(value = "customerDao")
public class CustomerDaoImpl implements CustomerDao {

	@Autowired(required = true)
	JdbcTemplate jdbcTemplate;
	
	private static Logger logger = Logger.getLogger(CustomerDaoImpl.class);

	@Override
	public void addCustomer(CustomerEntity customer) {
		try{
			String sql = "INSERT INTO tb_user(f_address,f_birthday,f_email,f_password,f_username) VALUES (?,?,?,?,?);";
			GeneratedKeyHolder keyHolder = new GeneratedKeyHolder(); 
			jdbcTemplate.update(new PreparedStatementCreator() {

				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement statement = connection.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
					statement.setString(1, customer.getAddress());
					statement.setDate(2, new Date(customer.getBirthday().getTime()));
					statement.setString(3, customer.getEmail());
					statement.setString(4, customer.getPassword());
					statement.setString(5, customer.getUsername());
					return statement;
				}
			},keyHolder);
			customer.setId(keyHolder.getKey().intValue());
		}catch(Exception e){
			logger.error("Exception",e);
		}
	}

	@Override
	public void updateCustomer(CustomerEntity customer) {
		String sql = "UPDATE tb_user SET f_address=?,f_birthday=?,f_email=?,f_password=?,f_username=? WHERE f_id = ?;";
		jdbcTemplate.update(sql, customer.getAddress(), customer.getBirthday(), customer.getEmail(),
				customer.getPassword(), customer.getUsername(), customer.getId());
	}

	@Override
	public void deleteCustomer(CustomerEntity customer) {
		String sql = "DELETE FROM tb_user  WHERE f_id = ?;";
		jdbcTemplate.update(sql, customer.getId());
	}

	@Override
	public CustomerEntity getCustomer(int id) {
		String sql = "SELECT * FROM tb_user WHERE f_id = ?";
		Object[] params = {id};
		List<CustomerEntity> customers = (List<CustomerEntity>) jdbcTemplate.query(sql, params,new RowMapper<CustomerEntity>() {
			@Override
			public CustomerEntity mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
				CustomerEntity customer = new CustomerEntity();
				customer.setAddress(resultSet.getString("f_address"));
				customer.setBirthday(resultSet.getDate("f_birthday"));
				customer.setEmail(resultSet.getString("f_email"));
				customer.setPassword(resultSet.getString("f_password"));
				customer.setUsername(resultSet.getString("f_username"));
				customer.setId(resultSet.getInt("f_id"));
				return customer;
			}
		});
		if(customers.isEmpty()){
			return null;
		}else{
			return customers.get(0);
		}
	}

}
