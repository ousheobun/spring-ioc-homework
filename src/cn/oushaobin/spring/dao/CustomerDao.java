package cn.oushaobin.spring.dao;

import cn.oushaobin.spring.entity.CustomerEntity;

public interface CustomerDao {
	
	public void addCustomer( CustomerEntity customer );
	
	public void updateCustomer( CustomerEntity customer );
	
	public void deleteCustomer( CustomerEntity customer );
	
	public CustomerEntity getCustomer(int id);

}
