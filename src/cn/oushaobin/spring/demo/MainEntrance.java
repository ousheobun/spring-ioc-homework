package cn.oushaobin.spring.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;
import cn.oushaobin.spring.base.SpringBeanFactory;
import cn.oushaobin.spring.dao.CustomerDao;
import cn.oushaobin.spring.entity.CustomerEntity;

/**
 * Spring IOC 实验测试入口
 * @author ousheobin
 * 请确保config/beans.xml中对数据库的配置正确
 * 并且确保目标数据库中存在数据表，tb_user,否则请允许下面的查询
 * 
	DROP TABLE IF EXISTS `tb_user`;
	CREATE TABLE `tb_user` (
		`f_id` int(11) NOT NULL AUTO_INCREMENT,
  		`f_address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  		`f_birthday` datetime DEFAULT NULL,
  		`f_email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  		`f_password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  		`f_username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  		PRIMARY KEY (`f_id`)
	) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

 */
public class MainEntrance {
	
	private CustomerDao customerDao;
	
	private static Logger logger = Logger.getLogger(MainEntrance.class);
	
	public MainEntrance(){
		customerDao = (CustomerDao) SpringBeanFactory.getBean("customerDao");
	}
	
	public static void main(String[] args){
		MainEntrance appliaction = new MainEntrance();
		try {
			appliaction.test();
		} catch (Exception e) {
			logger.error("Exception",e);
		}
	}
	
	public void test() throws ParseException{
		CustomerEntity customer = new CustomerEntity();
		SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd");
		customer.setId(11);
		customer.setAddress("Guangzhou College of SCUT,Huadu,Guangzhou,China");
		customer.setBirthday(spf.parse("1996-10-05"));
		customer.setEmail("me@oushaobin.cn");
		customer.setPassword("123456");
		customer.setUsername( "ousheobin");
		customerDao.addCustomer(customer);
		System.out.println(customerDao.getCustomer(customer.getId()));
	}

}
