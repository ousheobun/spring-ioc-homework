#Spring-IOC-Homework

这是王芳老师的Spring框架实现控制反转和依赖注入作业的样例


##使用的开发与测试环境##

- 使用Eclipse Neon2 开发，普通Java项目
- 数据库使用 MySQL 5.6，需要更换数据库的，请修改config/beans.xml
- 默认数据表 **crm_lite** 默认字符编码 UTF-8 Bin
- 默认日志框架 Log4j 日志级别为INFO

程序入口：cn.oushaobin.spring.demo.MainEntrance

## 数据库相关 ##
如果第一次运行，需要在目标数据库执行下列建表语句(MySQL5.6+InnoDB)
```
	DROP TABLE IF EXISTS `tb_user`;
	CREATE TABLE `tb_user` (
		`f_id` int(11) NOT NULL AUTO_INCREMENT,
  		`f_address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  		`f_birthday` datetime DEFAULT NULL,
  		`f_email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  		`f_password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  		`f_username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  		PRIMARY KEY (`f_id`)
	) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
```

## 关于Spring IOC控制的说明 ##

本作业里面使用了两种注入方式

一种是使用静态BeanFactory调用getBean()获取，例如
```
SpringBeanFactory.getBean("customerDao");
```

还有一种是使用注解@Autowired的方法注入，首先在beans.xml配置扫描路径
```
<context:component-scan base-package="cn.oushaobin.spring.dao.*"/>
```

然后再目标类加上@Service,@Component,@Repository等注解，声明为组件，例如：
```
@Repository(value = "customerDao")
public class CustomerDaoImpl implements CustomerDao {

  @Autowired(required = true)
  JdbcTemplate jdbcTemplate;
  ...

}
```
这时候，Spring会自动把CustomerDaoImpl作为一个Bean，并且完成对jdbcTemplate的自动注入
